//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParse =require('body-parser');
app.use(bodyParse.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type, Accept");
  next();
});

var requestjson=require('request-json');

var urlMovimientos="https://api.mlab.com/api/1/databases/abarrios/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab=requestjson.createClient(urlMovimientos);

var urlClientes="http://192.168.0.11:7050/customers/v1/customers?documentNumber=043547663&documentType=DNI";
var apiCliente=requestjson.createClient(urlClientes);

var urlUsuario="https://api.mlab.com/api/1/databases/abarrios/collections/usuario?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMlab=requestjson.createClient(urlUsuario);

var mensaje="";
var error=true;
var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})
//virtual banck
app.get('/clientesapi',function(req,res){
  apiCliente.get('',function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
});

let respuesta = {
 error: false,
 codigo: 200,
 mensaje: ''
};
let usuario = {};
//gestion login
app.post('/usuario/login', function (req, res) {
  console.log(req.body.dni);
  console.log(req.body.clave);
 if(!req.body.dni || !req.body.clave) {
    respuesta = {
     error: true,
     codigo: 502,
     mensaje: 'El campo dni y clave son requeridos'
    };
 } else {
   var filtro="{'dni':'"+req.body.dni+"','clave':'"+req.body.clave+"'}";
   usuario= {};
   var urlLogin=urlUsuario+"&q="+filtro;
   console.log(urlLogin);
   usuarioMlab=requestjson.createClient(urlLogin);
   usuarioMlab.get('',function(err,resM,body){
     if(err){
       console.log("error=>"+err);
     }else{
       console.log("body=>"+JSON.stringify(body));
       usuario=body;
     }
     console.log("usuario.length=>"+usuario.length);
     if(usuario.length==0){
       respuesta = {
        error: true,
        codigo: 500,
        mensaje: 'Credenciales incorrectas'
       };
     }else{
       respuesta = {
        error: false,
        codigo: 200,
        mensaje: 'Usuario Login ok',
        respuesta: usuario
       };
     }

     res.send(respuesta);
  });
  }
});
//consulta movimientos de clientes
app.get('/movimientos/:idcliente',function(req,res){
  var idCliente=req.params.idcliente;
  console.log("idCliente=>"+idCliente);
  var urlMoviCliente=urlMovimientos+"&q={'idcliente':"+idCliente+"}";
  console.log("clienteMlab=>"+urlMoviCliente);
  clienteMlab=requestjson.createClient(urlMoviCliente);
  clienteMlab.get('',function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }

  });
});
//Dar Alta movimientos
app.post('/movimientos',function(req,res){
  clienteMlab=requestjson.createClient(urlMovimientos);
  var mensaje="Procesando..";
  clienteMlab.post('',req.body,function(err,resM,bodyPost){
    if(err){
      console.log("Error al guardar");
      console.log(err);
      mensaje=err;
    }else{
      console.log("Guardado OK");
      mensaje="Guardado OK";
      error=false;
    }
    respuesta = {
     error: error,
     codigo: 200,
     mensaje: mensaje
    };
    res.send(respuesta);
  });




})

//Dar Alta Usuarios
app.post('/usuario',function(req,res){
  usuarioMlab=requestjson.createClient(urlUsuario);
  var mensaje="Procesando..";
  usuarioMlab.post('',req.body,function(err,resM,bodyPost){
    if(err){
      console.log("Error al guardar");
      console.log(err);
      mensaje=err;
    }else{
      console.log("Guardado OK");
      mensaje="Guardado OK";
      error=false;
    }
    respuesta = {
     error: error,
     codigo: 200,
     mensaje: mensaje
    };
    res.send(respuesta);
  });
});
